package ma.cimr.contrat.rest.editique;

import java.io.FileNotFoundException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ma.cimr.contrat.common.Constants;
import ma.cimr.contrat.service.IBdocService;


@RestController
@RequestMapping("/editique")
public class EditiqueController {
	
	private static final Logger logger = LoggerFactory.getLogger(EditiqueController.class);

	@Autowired
	private IBdocService bdocService;
	
	@PostMapping("/printBulletin")
	public ResponseEntity<String> printBulletin(@RequestBody Map<String, String> parameters) {
		
		JSONObject response = new JSONObject();
		String content = null;
		try {
//			JSONObject parameters = new JSONObject(request);
			content = bdocService.printBulletin(parameters);
			response.put(Constants.CONTENT, content);
			
			return new ResponseEntity<String>(response.toString(), HttpStatus.CREATED);
			
		} catch (FileNotFoundException e) {
			logger.error("Erreur au niveau de la méthode printBulletin() : ", e);
			e.printStackTrace();
		} catch (JSONException e) {
			logger.error("Erreur au niveau de la méthode printBulletin() : ", e);
			e.printStackTrace();
		}
		
		return new ResponseEntity<String>(response.toString(), HttpStatus.EXPECTATION_FAILED);
	}
	
	@PostMapping("/printConvention")
	public ResponseEntity<String> printConvention(@RequestBody Map<String, String> parameters) {
		
		JSONObject response = new JSONObject();
		String content = null;
		try {
//			JSONObject parameters = new JSONObject(request);
			content = bdocService.printConvention(parameters);
			response.put(Constants.CONTENT, content);
			
			return new ResponseEntity<String>(response.toString(), HttpStatus.CREATED);
			
		} catch (FileNotFoundException e) {
			logger.error("Erreur au niveau de la méthode printConvention() : ", e);
			e.printStackTrace();
		} catch (JSONException e) {
			logger.error("Erreur au niveau de la méthode printConvention() : ", e);
			e.printStackTrace();
		}
		
		return new ResponseEntity<String>(response.toString(), HttpStatus.EXPECTATION_FAILED);
	}
	
	
}
