package ma.cimr.contrat.config;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ma.cimr.contrat.common.Constants;

public abstract class ApplicationConfig {

	private static final Logger logger = LoggerFactory.getLogger(ApplicationConfig.class);
    public static Properties prop = new Properties();
	
	static {
		InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(Constants.CONFIG_FILE);
		try{
		if (inputStream != null) {
			prop.load(inputStream);
		} else {
			throw new FileNotFoundException("property file '" + Constants.CONFIG_FILE + "' not found in the classpath");
		}
		} catch (IOException e) {
			logger.error("error while getting properties ...", e);
		}
	}
	
	public static String getUrlBdocOnDemand(){
		return prop.getProperty(Constants.URL_BDOC_ON_DEMAND);
	}
	
	public static String getUrlBdocDocumentContent(){
		return prop.getProperty(Constants.URL_BDOC_DOCUMENT_CONTENT);
	}
	
	public static String getConventionXmlFileNamePath(){
		return prop.getProperty(Constants.CONVENTION_XML_FILE_NAME_PATH);
	}
	
	public static String getConventionXmlFileNameTemplate(){
		return prop.getProperty(Constants.CONVENTION_XML_FILE_NAME_TEMPLATE);
	}
	
	public static String getBulletinXmlFileNamePath(){
		return prop.getProperty(Constants.BULLETIN_XML_FILE_NAME_PATH);
	}
	
	public static String getBulletinXmlFileNameTemplate(){
		return prop.getProperty(Constants.BULLETIN_XML_FILE_NAME_TEMPLATE);
	}
}

