package ma.cimr.contrat.util;

import java.util.List;

public class CollectionUtil {

	public static boolean isEmpty(List<?> str) {
		
		if(str == null) {
			return true;
		}
		if(str.size() == 0) {
			return true;
		}
		return false;
	}
	
	public static boolean isNotEmpty(List<?> str) {
		return !isEmpty(str);
	}
}
