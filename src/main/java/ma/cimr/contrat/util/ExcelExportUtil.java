/* package ma.cimr.contrat.util;

import java.io.IOException;
import java.io.OutputStream;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.ScriptStyle;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public class ExcelExportUtil {	
	public static boolean generate(String name,List<Object> data,List<Object> titles,OutputStream out)
	{
		
		try {
			
			
			int count_t = 0;
			int count_data = 1;
			int count_data_list = 0;
			
		WritableWorkbook workbook = Workbook.createWorkbook(out);

		WritableSheet sheet = workbook.createSheet(name, 0);

		WritableFont titlesfont = new WritableFont(WritableFont.ARIAL, 13,WritableFont.BOLD, true, UnderlineStyle.NO_UNDERLINE,Colour.BLACK, ScriptStyle.NORMAL_SCRIPT);
		WritableFont contenuFont = new WritableFont(WritableFont.ARIAL, 11,WritableFont.NO_BOLD, true, UnderlineStyle.NO_UNDERLINE,Colour.BLACK, ScriptStyle.NORMAL_SCRIPT);
		
		
		WritableCellFormat titlesformat = new WritableCellFormat(titlesfont);
		WritableCellFormat contenuFormat = new WritableCellFormat(contenuFont);
		
		titlesformat.setBackground(Colour.GREY_25_PERCENT);
		titlesformat.setAlignment(Alignment.CENTRE);
		contenuFormat.setShrinkToFit(false);

		
	    Iterator it = titles.iterator();
	    
	    while(it.hasNext())
	    {
	    String title = (String) it.next();
		Label label = new Label(count_t, 0, title,titlesformat);
		sheet.addCell(label);
		count_t++;
	    }

	    
	    Iterator itl = data.iterator();
	    while(itl.hasNext())
	    {
	    	List data_list = (List) itl.next();
	    	Iterator dti = data_list.iterator();
	    	while(dti.hasNext())
	    	{
	    		String data_string = (String) dti.next();
	    		if(count_data_list!=3 || !NumberUtils.isNumber(data_string)) {
	    		Label label2 = new Label(count_data_list,
	    				count_data,
	    				data_string,contenuFormat);
	    		sheet.addCell(label2);
	    		} else { 
	    		jxl.write.Number label2 = null;
				try {
					label2 = new jxl.write.Number(count_data_list,
								count_data,
								NumberFormat.getInstance().parse(data_string).doubleValue(),contenuFormat);
					sheet.addCell(label2);
				} catch (ParseException e) {
					Label label3 = new Label(count_data_list,
		    				count_data,
		    				data_string,contenuFormat);
		    		sheet.addCell(label3);
				}
	    		
	    		}
	    		count_data_list++;
	    	}
	    	count_data++;
	    	count_data_list=0;
	    }
		workbook.write();
		workbook.close();
		out.close();
		return true;

		} catch (RowsExceededException e1) {

		e1.printStackTrace();
		return false;

		} catch (WriteException e1) {

		e1.printStackTrace();
		return false;

		} catch (IOException e) {

		e.printStackTrace();
		return false;

	    }
	}
	
	public static org.apache.poi.ss.usermodel.Workbook getWorkbook(String excelFilePath)throws IOException {
		org.apache.poi.ss.usermodel.Workbook workbook = null;
	 
	    if (excelFilePath.endsWith("xlsx")) {
	    	workbook = new XSSFWorkbook();
	    } else if (excelFilePath.endsWith("xls")) {
	        workbook = new HSSFWorkbook();
	    } else {
	        throw new IllegalArgumentException("The specified file is not Excel file");
	    }
	 
	    return workbook;
	}
	
	public static void createHeaderRow(Sheet sheet, String[] header) {
		 
	    CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
	    Font font = sheet.getWorkbook().createFont();
	    font.setBold(true);
	    font.setFontHeightInPoints((short) 12);
	    cellStyle.setFont(font);
	    cellStyle.setFillBackgroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
	    
	    Row row = sheet.createRow(0);
	   
	    
	    for(int i = 0; i < header.length; i++) {
	    	Cell cell = row.createCell(i);
	    	cell.setCellStyle(cellStyle);
	    	cell.setCellValue(header[i]);
	    }
	    
	}
	
	public static void main(String ...strings) {
	}

}
*/