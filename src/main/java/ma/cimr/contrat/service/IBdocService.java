package ma.cimr.contrat.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;

import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.w3c.dom.DOMException;

public interface IBdocService {
	
	public String printConvention(Map<String, String> parameters) throws FileNotFoundException, JSONException;
	
	public String printBulletin(Map<String, String> parameters) throws FileNotFoundException, JSONException;
	
	public String printBdocOnDemand(File flux, String templateName) throws FileNotFoundException, JSONException;

	public JSONObject post(JSONObject request, String urlString) throws JSONException;
	
	public JSONObject get(String urlString) throws JSONException;
	
	public byte[] parametrerXmlConvention(Map<String, String> parameters);
	
	public byte[] parametrerXmlBulletin(Map<String, String> parameters);
}
