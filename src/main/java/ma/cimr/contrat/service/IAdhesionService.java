package ma.cimr.contrat.service;

import java.util.List;
import java.util.Map;

import ma.cimr.contrat.service.dto.DemandeAdhesionDto;

public interface IAdhesionService {

	public List<DemandeAdhesionDto> getDemandesAdhesionValidateur();
	
	public List<DemandeAdhesionDto> getDemandesAdhesionApprobateur();
	
	public Map<String, Object> getDetailDemande(Long id);
	
	public Boolean validateDemande(Long id);
	
	public Boolean rejectDemande(Long id, String motif);
	
}
