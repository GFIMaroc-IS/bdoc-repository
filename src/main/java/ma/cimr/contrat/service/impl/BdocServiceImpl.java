package ma.cimr.contrat.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import ma.cimr.contrat.common.Constants;
import ma.cimr.contrat.config.ApplicationConfig;
import ma.cimr.contrat.service.IBdocService;
import ma.cimr.contrat.util.FileUtil;

@Service
public class BdocServiceImpl implements IBdocService {

	private static final Logger logger = LoggerFactory.getLogger(BdocServiceImpl.class);
	
	@Override
	public String printConvention(Map<String, String> parameters) throws FileNotFoundException, JSONException {

//		Map<String, String> parameters = (String)request.get(Constants.PARAMS_MAP);
		parametrerXmlConvention(parameters);

//		InputStream is = getClass().getResourceAsStream(ApplicationConfig.getConventionXmlFileNamePath());
		
		
		File flux = ResourceUtils.getFile(ApplicationConfig.getConventionXmlFileNamePath());
		String conventionContent = printBdocOnDemand(flux, Constants.TEMPLATE_NAME_CONVENTION);
		
		return conventionContent;
	}
	
	@Override
	public String printBulletin(Map<String, String> parameters) throws FileNotFoundException, JSONException {

		parametrerXmlBulletin(parameters);

		//InputStream is = getClass().getResourceAsStream(ApplicationConfig.getBulletinXmlFileNamePath());


		File flux = ResourceUtils.getFile(ApplicationConfig.getBulletinXmlFileNamePath());
		String bulletinContent = printBdocOnDemand(flux, Constants.TEMPLATE_NAME_BULLETIN);
		
		return bulletinContent;
	}



	public String printBdocOnDemand(InputStream flux, String templateName) throws FileNotFoundException, JSONException {
		
		
		String fluxEncoded = FileUtil.encodeStreamToBase64(flux);	
		String urlBdocOnDemand = ApplicationConfig.getUrlBdocOnDemand();
		
		JSONObject request = new JSONObject();
		request.put("correlationId", "TEST CIMR");
		request.put("templateName", templateName);
		request.put("isDatastreamReadFromFile", false);
		request.put("documentFormat", "PDF");
		request.put("assembleIf", false);
		request.put("datastreamContentAsBase64", fluxEncoded);
		request.put("customDocFlowName", "");
		request.put("keepDocumentsInSpool", false);
		request.put("timeout", 0);
		request.put("priority", 0);
		
		JSONObject documentIdJson = post(request, urlBdocOnDemand);
		JSONObject documents = documentIdJson.getJSONArray("documents").getJSONObject(0);
		String id = documents.getString("id");
		
		String urlDocument = ApplicationConfig.getUrlBdocDocumentContent() + id + "/content";
		
		JSONObject documentContentJson = get(urlDocument);
		String content = documentContentJson.getString("content");
//		byte[] document = FileUtil.decodeFileToBase64(content);
		
		return content;
	}



	@Override
	public String printBdocOnDemand(File flux, String templateName) throws FileNotFoundException, JSONException {
		
		
		String fluxEncoded = FileUtil.encodeFileToBase64(flux);
		
		String urlBdocOnDemand = ApplicationConfig.getUrlBdocOnDemand();
		
		JSONObject request = new JSONObject();
		request.put("correlationId", "TEST CIMR");
		request.put("templateName", templateName);
		request.put("isDatastreamReadFromFile", false);
		request.put("documentFormat", "PDF");
		request.put("assembleIf", false);
		request.put("datastreamContentAsBase64", fluxEncoded);
		request.put("customDocFlowName", "");
		request.put("keepDocumentsInSpool", false);
		request.put("timeout", 0);
		request.put("priority", 0);
		
		JSONObject documentIdJson = post(request, urlBdocOnDemand);
		JSONObject documents = documentIdJson.getJSONArray("documents").getJSONObject(0);
		String id = documents.getString("id");
		
		String urlDocument = ApplicationConfig.getUrlBdocDocumentContent() + id + "/content";
		
		JSONObject documentContentJson = get(urlDocument);
		String content = documentContentJson.getString("content");
//		byte[] document = FileUtil.decodeFileToBase64(content);
		
		return content;
	}

	public JSONObject post(JSONObject request, String urlString) throws JSONException {
		
		JSONObject response = null;
		RestTemplate restTemplate = new RestTemplate();
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

		// send request and parse result
		ResponseEntity<String> apiResponse = restTemplate.exchange(urlString, HttpMethod.POST, entity, String.class);
		if (apiResponse.getStatusCode() == HttpStatus.CREATED) {
			response = new JSONObject(apiResponse.getBody());
		}
		
		return response;
	}
	
	public JSONObject get(String urlString) throws JSONException {
		
		JSONObject response = null;
		RestTemplate restTemplate = new RestTemplate();
		
		ResponseEntity<String> apiResponse = restTemplate.getForEntity(urlString, String.class);
		if (apiResponse.getStatusCode() == HttpStatus.OK) {
			response = new JSONObject(apiResponse.getBody());
		}
		
		return response;
	}
	
	@Override
	public byte[] parametrerXmlConvention(Map<String, String> parameters) {

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		
		Document doc = null;
		
		byte[] modifiedXML = null;
		logger.warn("-- Début de mise à jour du fichier XML--");
		logger.warn("----> Chemin resource : " + ApplicationConfig.getConventionXmlFileNamePath());


		

		try (InputStream is = new FileInputStream(Constants.XML_PATH + ApplicationConfig.getConventionXmlFileNameTemplate())) {
			logger.warn("----> is : " + is);
		
			DocumentBuilder db = dbf.newDocumentBuilder();

			doc = db.parse(is);

			Node entreprise = doc.getElementsByTagName("entreprise").item(0);
			if (entreprise.getNodeType() == Node.ELEMENT_NODE) {

				NodeList childNodes = entreprise.getChildNodes();
				for (int j = 0; j < childNodes.getLength(); j++) {
					Node item = childNodes.item(j);
					if ("denomination".equalsIgnoreCase(item.getNodeName())) {
						item.setTextContent(parameters.get("denomination"));
					}
					if ("rs".equalsIgnoreCase(item.getNodeName())) {
						item.setTextContent(parameters.get("rs"));
					}

				}

			}
			
			Node infos = doc.getElementsByTagName("info_globales").item(0);
			if (infos.getNodeType() == Node.ELEMENT_NODE) {

				NodeList childNodes = infos.getChildNodes();
				for (int j = 0; j < childNodes.getLength(); j++) {
					Node item = childNodes.item(j);
					if ("lieu".equalsIgnoreCase(item.getNodeName())) {
						item.setTextContent(parameters.get("lieu"));
					}
					if ("date_cse".equalsIgnoreCase(item.getNodeName())) {
						item.setTextContent(parameters.get("date_cse"));
					}
					if ("marque".equalsIgnoreCase(item.getNodeName())) {
						item.setTextContent(parameters.get("marque"));
					}
					if ("langue".equalsIgnoreCase(item.getNodeName())) {
						item.setTextContent(parameters.get("langue"));
					}

				}

			}
			
			Node infosGest = doc.getElementsByTagName("gestionnaire_compte_adherent").item(0);
			if (infosGest.getNodeType() == Node.ELEMENT_NODE) {

				NodeList childNodes = infosGest.getChildNodes();
				for (int j = 0; j < childNodes.getLength(); j++) {
					Node item = childNodes.item(j);
					if ("nom".equalsIgnoreCase(item.getNodeName())) {
						item.setTextContent(parameters.get("nom"));
					}
					if ("prenom".equalsIgnoreCase(item.getNodeName())) {
						item.setTextContent(parameters.get("prenom"));
					}

				}

			}

			FileOutputStream output = new FileOutputStream(ApplicationConfig.getConventionXmlFileNamePath());
			writeXml(doc, output);
			
//			modifiedXML = documentToByte(doc);
			
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return modifiedXML;

	}
	
	@Override
	public byte[] parametrerXmlBulletin(Map<String, String> parameters){

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		
		Document doc = null;
		
		byte[] modifiedXML = null;

		try (InputStream is = new FileInputStream(Constants.XML_PATH + ApplicationConfig.getBulletinXmlFileNameTemplate())) {	
			DocumentBuilder db = dbf.newDocumentBuilder();

			doc = db.parse(is);

			Node entreprise = doc.getElementsByTagName("entreprise").item(0);
			if (entreprise.getNodeType() == Node.ELEMENT_NODE) {

				NodeList childNodes = entreprise.getChildNodes();
				for (int j = 0; j < childNodes.getLength(); j++) {
					Node item = childNodes.item(j);
					if ("denomination".equalsIgnoreCase(item.getNodeName())) {
						item.setTextContent(parameters.get("denomination"));
					}
					if ("rs".equalsIgnoreCase(item.getNodeName())) {
						item.setTextContent(parameters.get("rs"));
					}

				}

			}
			
			Node infos = doc.getElementsByTagName("info_globales").item(0);
			if (infos.getNodeType() == Node.ELEMENT_NODE) {

				NodeList childNodes = infos.getChildNodes();
				for (int j = 0; j < childNodes.getLength(); j++) {
					Node item = childNodes.item(j);
					if ("lieu".equalsIgnoreCase(item.getNodeName())) {
						item.setTextContent(parameters.get("lieu"));
					}
					if ("date_cse".equalsIgnoreCase(item.getNodeName())) {
						item.setTextContent(parameters.get("date_cse"));
					}
					if ("marque".equalsIgnoreCase(item.getNodeName())) {
						item.setTextContent(parameters.get("marque"));
					}
					if ("langue".equalsIgnoreCase(item.getNodeName())) {
						item.setTextContent(parameters.get("langue"));
					}

				}

			}
			
			Node infosGest = doc.getElementsByTagName("salarie").item(0);
			if (infosGest.getNodeType() == Node.ELEMENT_NODE) {

				NodeList childNodes = infosGest.getChildNodes();
				for (int j = 0; j < childNodes.getLength(); j++) {
					Node item = childNodes.item(j);
					if ("nom".equalsIgnoreCase(item.getNodeName())) {
						item.setTextContent(parameters.get("nom"));
					}
					if ("prenom".equalsIgnoreCase(item.getNodeName())) {
						item.setTextContent(parameters.get("prenom"));
					}

				}

			}

			FileOutputStream output = new FileOutputStream(ApplicationConfig.getBulletinXmlFileNamePath());
			writeXml(doc, output);
			
//			modifiedXML = documentToByte(doc);
			
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return modifiedXML;

	}
	
	private void writeXml(Document doc, OutputStream output) throws TransformerException, UnsupportedEncodingException {

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();

		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(output);

		transformer.transform(source, result); 
		
	}
}
