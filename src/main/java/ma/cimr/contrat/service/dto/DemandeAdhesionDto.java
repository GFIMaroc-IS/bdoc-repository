package ma.cimr.contrat.service.dto;

public class DemandeAdhesionDto {

	private Long id;
	private String numAdhesion;
	private String raisonSociale;
	private String dateEffetSouscription;
	private Integer statut;
	private String statutLibelle;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRaisonSociale() {
		return raisonSociale;
	}

	public void setRaisonSociale(String raisonSociale) {
		this.raisonSociale = raisonSociale;
	}

	public String getNumAdhesion() {
		return numAdhesion;
	}

	public void setNumAdhesion(String numAdhesion) {
		this.numAdhesion = numAdhesion;
	}

	public String getDateEffetSouscription() {
		return dateEffetSouscription;
	}

	public void setDateEffetSouscription(String dateEffetSouscription) {
		this.dateEffetSouscription = dateEffetSouscription;
	}

	public Integer getStatut() {
		return statut;
	}

	public void setStatut(Integer statut) {
		this.statut = statut;
	}

	public String getStatutLibelle() {
		return statutLibelle;
	}

	public void setStatutLibelle(String statutLibelle) {
		this.statutLibelle = statutLibelle;
	}

	

}
