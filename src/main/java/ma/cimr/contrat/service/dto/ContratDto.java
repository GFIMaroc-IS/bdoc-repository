package ma.cimr.contrat.service.dto;

import java.util.List;

public class ContratDto {

	private Long id;
	private String dateEffetSouscription;
	private Integer effectif;
	private String titulaire;
	private String rib;
	private String modePaiementCode;
	private String modePaiementLibelle;
	private String produitCode;
	private String produitLibelle;
	private List<DelegataireDto> delegataires;
	private Integer statut;
	private String statutLibelle;
	private String motifRejet;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDateEffetSouscription() {
		return dateEffetSouscription;
	}
	public void setDateEffetSouscription(String dateEffetSouscription) {
		this.dateEffetSouscription = dateEffetSouscription;
	}
	public Integer getEffectif() {
		return effectif;
	}
	public void setEffectif(Integer effectif) {
		this.effectif = effectif;
	}
	public String getTitulaire() {
		return titulaire;
	}
	public void setTitulaire(String titulaire) {
		this.titulaire = titulaire;
	}
	public String getRib() {
		return rib;
	}
	public void setRib(String rib) {
		this.rib = rib;
	}
	public String getModePaiementCode() {
		return modePaiementCode;
	}
	public void setModePaiementCode(String modePaiementCode) {
		this.modePaiementCode = modePaiementCode;
	}
	public String getModePaiementLibelle() {
		return modePaiementLibelle;
	}
	public void setModePaiementLibelle(String modePaiementLibelle) {
		this.modePaiementLibelle = modePaiementLibelle;
	}
	public String getProduitCode() {
		return produitCode;
	}
	public void setProduitCode(String produitCode) {
		this.produitCode = produitCode;
	}
	public String getProduitLibelle() {
		return produitLibelle;
	}
	public void setProduitLibelle(String produitLibelle) {
		this.produitLibelle = produitLibelle;
	}
	public List<DelegataireDto> getDelegataires() {
		return delegataires;
	}
	public void setDelegataires(List<DelegataireDto> delegataires) {
		this.delegataires = delegataires;
	}
	public Integer getStatut() {
		return statut;
	}
	public void setStatut(Integer statut) {
		this.statut = statut;
	}
	public String getStatutLibelle() {
		return statutLibelle;
	}
	public void setStatutLibelle(String statutLibelle) {
		this.statutLibelle = statutLibelle;
	}
	public String getMotifRejet() {
		return motifRejet;
	}
	public void setMotifRejet(String motifRejet) {
		this.motifRejet = motifRejet;
	}

}
