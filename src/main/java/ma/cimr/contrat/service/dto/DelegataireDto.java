package ma.cimr.contrat.service.dto;

import java.util.ArrayList;
import java.util.List;

import ma.cimr.contrat.util.StringUtil;

public class DelegataireDto{

	private Long id;
	private String nom;
	private String prenom;
	private String cin;
	private String fonction;
	private String categorieHabilitationCode;
	private String categorieHabilitationLibelle;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getCin() {
		return cin;
	}

	public void setCin(String cin) {
		this.cin = cin;
	}

	public String getFonction() {
		return fonction;
	}

	public void setFonction(String fonction) {
		this.fonction = fonction;
	}

	public String getCategorieHabilitationCode() {
		return categorieHabilitationCode;
	}

	public void setCategorieHabilitationCode(String categorieHabilitationCode) {
		this.categorieHabilitationCode = categorieHabilitationCode;
	}

	public String getCategorieHabilitationLibelle() {
		return categorieHabilitationLibelle;
	}

	public void setCategorieHabilitationLibelle(String categorieHabilitationLibelle) {
		this.categorieHabilitationLibelle = categorieHabilitationLibelle;
	}
	
	public static boolean isEmpty(DelegataireDto dto) {
		if(dto.getId() == null && StringUtil.isEmpty(dto.getNom()) && StringUtil.isEmpty(dto.getPrenom())
				&& StringUtil.isEmpty(dto.getCin()) && StringUtil.isEmpty(dto.getFonction()) 
				&& StringUtil.isEmpty(dto.getCategorieHabilitationCode()))
			return true;
		
		return false;
	}
	
	public static List<DelegataireDto> list(List<DelegataireDto> listDtos){
		List<DelegataireDto> list = new ArrayList<>();
		
		if(listDtos == null)
			return listDtos;
		for(DelegataireDto dto : listDtos) {
			if(!isEmpty(dto)) {
				list.add(dto);
			}
		}
		
		return list;
	}
	
}
